﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Skeleton.Api.Infrastructure.SwaggerComponent;
using Skeleton.Api.Infrastructure.TodoComponent;

namespace Skeleton.Api
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Config = config;
        }

        public IConfiguration Config { get; }

        public void ConfigureServices(IServiceCollection svc)
        {
            svc.AddTodoServiceFabricProxy();
            svc.AddSwaggerConfiguration();
            svc.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwaggerConfiguration();
            app.UseMvc();
        }
    }
}