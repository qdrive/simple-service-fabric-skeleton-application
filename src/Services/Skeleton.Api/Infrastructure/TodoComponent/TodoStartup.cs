﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Microsoft.ServiceFabric.Services.Remoting.V2.FabricTransport.Client;
using Skeleton.TodoService.Domain;
using System;

namespace Skeleton.Api.Infrastructure.TodoComponent
{
    public static class TodoStartup
    {
        public static void AddTodoServiceFabricProxy(this IServiceCollection svc)
        {
            var proxy = new ServiceProxyFactory(ctx => new FabricTransportServiceRemotingClientFactory());

            svc.AddScoped<ITodoService>(p => proxy.CreateServiceProxy<ITodoService>(new Uri("fabric:/Skeleton/Skeleton.TodoService"), new ServicePartitionKey(0)));
        }
    }
}