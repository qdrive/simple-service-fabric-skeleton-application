﻿using Skeleton.TodoService.Domain;

namespace Skeleton.Api.Infrastructure.TodoComponent
{
    internal static class TodoMap
    {
        public static Todo ToDomain(this TodoModel todoModel)
        {
            return new Todo
            {
                Description = todoModel.Description,
                Id = todoModel.Id,
                Name = todoModel.Name,
                Score = todoModel.Score
            };
        }

        public static TodoModel ToModel(this Todo todo)
        {
            return new TodoModel
            {
                Description = todo.Description,
                Id = todo.Id,
                Name = todo.Name,
                Score = todo.Score
            };
        }
    }
}
