﻿using Newtonsoft.Json;
using System;

namespace Skeleton.Api.Infrastructure.TodoComponent
{
    public class TodoModel
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }
    }
}
