﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Skeleton.TodoService.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Skeleton.Api.Infrastructure.TodoComponent
{
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
        private readonly ITodoService _svc;

        public TodoController(ITodoService svc)
        {
            _svc = svc;
        }

        [HttpGet]
        public async Task<IEnumerable<TodoModel>> Get()
        {
            var data = await _svc.GetTodos();

            return data.Select(m => m.ToModel());
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var data = await _svc.GetTodo(id);

            if (data != null)
            {
                return Ok(data);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TodoModel todoModel)
        {
            await _svc.SaveTodo(todoModel.ToDomain());
            return StatusCode(201);
        }

        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> Patch(Guid id, [FromBody]JsonPatchDocument<TodoModel> todoPatch)
        {
            var todo = await _svc.GetTodo(id);

            if (todo == null)
            {
                return NotFound();
            }
            try
            {
                var todoModel = todo.ToModel();

                todoPatch.ApplyTo(todoModel);
                await _svc.SaveTodo(todoModel.ToDomain());
                return Ok(todoModel);
            }
            catch (JsonPatchException ex)
            {
                return BadRequest($"An error occured whilst updating todo. {ex.Message}.");
            }
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (await _svc.GetTodo(id) == null)
            {
                return NotFound();
            }
            await _svc.DeleteTodo(id);
            return NoContent();
        }
    }
}