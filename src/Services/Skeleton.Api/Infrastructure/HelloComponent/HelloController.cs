﻿using Microsoft.AspNetCore.Mvc;

namespace Skeleton.Api.Infrastructure.HelloComponent
{
    [Route("api/[controller]")]
    public class HelloController : Controller
    {
        [HttpGet]
        public string Get()
        {
            return "Hello World!";
        }
    }
}