﻿using Microsoft.ServiceFabric.Services.Remoting;
using System;
using System.Threading.Tasks;

namespace Skeleton.TodoService.Domain
{
    public interface ITodoService : IService
    {
        Task SaveTodo(Todo todo);

        Task<Todo> GetTodo(Guid id);

        Task<Todo[]> GetTodos();

        Task DeleteTodo(Guid id);
    }
}