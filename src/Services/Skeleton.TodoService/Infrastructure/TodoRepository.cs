﻿using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using Skeleton.TodoService.Domain;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Skeleton.TodoService.Infrastructure
{
    public class TodoRepository : ITodoRepository
    {
        private readonly IReliableStateManager _stateManager;
        private const string DictionaryName = "todos";

        public TodoRepository(IReliableStateManager stateManager)
        {
            _stateManager = stateManager;
        }

        public async Task SaveTodo(Todo todo)
        {
            var todos = await _stateManager.GetOrAddAsync<IReliableDictionary<Guid, Todo>>(DictionaryName);

            using (var tx = _stateManager.CreateTransaction())
            {
                await todos.AddOrUpdateAsync(tx, todo.Id = Guid.NewGuid(), todo, (guid, value) => todo = value);
                await tx.CommitAsync();
            }
        }

        public async Task<Todo> GetTodo(Guid id)
        {
            var todos = await _stateManager.GetOrAddAsync<IReliableDictionary<Guid, Todo>>(DictionaryName);

            using (var tx = _stateManager.CreateTransaction())
            {
                var todo = await todos.TryGetValueAsync(tx, id);
                return todo.HasValue ? todo.Value : null;
            }
        }

        public async Task<Todo[]> GetTodos()
        {
            var todos = await _stateManager.GetOrAddAsync<IReliableDictionary<Guid, Todo>>(DictionaryName);
            var result = new List<Todo>();

            using (var tx = _stateManager.CreateTransaction())
            {
                var enumerable = await todos.CreateEnumerableAsync(tx, EnumerationMode.Unordered);

                using (var enumerator = enumerable.GetAsyncEnumerator())
                {
                    while (await enumerator.MoveNextAsync(CancellationToken.None))
                    {
                        KeyValuePair<Guid, Todo> current = enumerator.Current;
                        result.Add(current.Value);
                    }
                }
            }

            return result.ToArray();
        }

        public async Task DeleteTodo(Guid id)
        {
            var todos = await _stateManager.GetOrAddAsync<IReliableDictionary<Guid, Todo>>(DictionaryName);

            using (var tx = _stateManager.CreateTransaction())
            {
                await todos.TryRemoveAsync(tx, id);
                await tx.CommitAsync();
            }
        }
    }
}