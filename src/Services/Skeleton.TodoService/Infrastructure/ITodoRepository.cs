﻿using Skeleton.TodoService.Domain;
using System;
using System.Threading.Tasks;

namespace Skeleton.TodoService.Infrastructure
{
    public interface ITodoRepository
    {
        Task SaveTodo(Todo todo);

        Task<Todo> GetTodo(Guid id);

        Task<Todo[]> GetTodos();

        Task DeleteTodo(Guid id);
    }
}