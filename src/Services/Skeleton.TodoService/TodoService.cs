﻿using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Remoting.V2.FabricTransport.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using Skeleton.TodoService.Domain;
using Skeleton.TodoService.Infrastructure;
using System;
using System.Collections.Generic;
using System.Fabric;
using System.Threading.Tasks;

namespace Skeleton.TodoService
{
    internal sealed class TodoService : StatefulService, ITodoService
    {
        private readonly ITodoRepository _repo;

        public TodoService(StatefulServiceContext ctx): base(ctx)
        {
            _repo = new TodoRepository(StateManager);
        }

        protected override IEnumerable<ServiceReplicaListener> CreateServiceReplicaListeners()
        {
            return new[]
            {
                new ServiceReplicaListener(ctx => new FabricTransportServiceRemotingListener(ctx, this))
            };
        }

        public async Task SaveTodo(Todo todo)
        {
            await _repo.SaveTodo(todo);
        }

        public async Task<Todo> GetTodo(Guid id)
        {
            return await _repo.GetTodo(id);
        }

        public async Task<Todo[]> GetTodos()
        {
            return await _repo.GetTodos();
        }

        public async Task DeleteTodo(Guid id)
        {
            await _repo.DeleteTodo(id);
        }
    }
}