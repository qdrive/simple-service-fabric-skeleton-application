# Simple Service Fabric Skeleton Application

This is just a simple barebones application which has been built to help me understand how Service Fabric operates and how to leverage the features that are available. If you are looking for a definitive guide to Service Fabric then a good starting point is [here](https://docs.microsoft.com/en-us/azure/service-fabric). I **only** use this for hacking around on a local cluster so expect some flaws.

## Frameworks and SDK's

| Name                            | Version       |
| :------------------------------ |:------------- |
| .NET Core                       | 2.1.302       |
| Azure Service Fabric            | 6.3.162.9494  |
| Azure Service Fabric SDK        | 3.2.162.9494  |

## What's Included

+ Basic Todo CRUD functionality has been implemented to demonstrate saving data to a persisted store.
+ The [Swagger](https://swagger.io) SDK has been used to invoke the API requests although any other similar client will achieve the same results.

## Getting Started

> Make sure a Service Fabric [Development Environment](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-get-started) is up and running.

With elevated privilages open the **Skeleton.ServiceFabric.sln** solution in Visual Studio. There will be a few projects in there:

+ **Skeleton** - This is where all of the application configuration lives.
+ **Skeleton.Api** - This is the stateless layer of the application where the HTTP endpoints are exposed.
+ **Skeleton.TodoService** - This is the stateful service tier where the Todo data is stored.
+ **Skeleton.TodoService.Domain** - These are the POCO classes for the Todo data layer.

Debug the applcation as you normally would and wait for the services to be deployed. After a few moments the **Build Output Console** will return a message similar to this:

```
The application is ready.
```

To take a deeper dive into the status of the running application(s), head on over to **http://localhost:19080/Explorer/index.html** to launch the [Service Fabric Explorer](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-visualizing-your-cluster).

![Service Fabric Explorer](assets/img/service-fabric-explorer.png)
## Adding Persited Data Examples

In a browser navigate to **http://localhost:19741/swagger** to launch Swagger to start invoking the CRUD methods.

### POST /api/todo

> Creates a Todo item.

Value:

```
{
  "name": "my first todo",
  "description": "just something awesome",
  "score": 100
}
```

### GET /api/todo

> Gets all Todo items.

Response body:

```
[
  {
    "id": "ac6ce488-1f1f-4d41-9577-b0c349058c4e",
    "name": "my first todo",
    "description": "just something awesome",
    "score": 100
  }
```

### GET /api/todo/{id}

> Gets a Todo item by Id.

```
id: ac6ce488-1f1f-4d41-9577-b0c349058c4e
```

Response body:

```
{
  "id": "ac6ce488-1f1f-4d41-9577-b0c349058c4e",
  "name": "my first todo",
  "description": "just something awesome",
  "score": 100
}
```

### PATCH /api/todo

> Updates a Todo item.

```
id: ac6ce488-1f1f-4d41-9577-b0c349058c4e
```

Value:

```
[
  {
    "op": "replace",
    "value": 2000,
    "path": "/score"
  }
]
```

Response body:

```
{
  "id": "ac6ce488-1f1f-4d41-9577-b0c349058c4e",
  "name": "my first todo",
  "description": "just something awesome",
  "score": 2000
}
```

### DELETE /api/todo/{id}

> Deletes a Todo item.

```
id: ac6ce488-1f1f-4d41-9577-b0c349058c4e
```

